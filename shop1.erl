-module(shop1).
-export([total/1]).

total([]) -> 0;
total([{Name, Quantity}|T]) ->
  shop:cost(Name) * Quantity + total(T).
