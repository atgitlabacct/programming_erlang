-module(patient).
-export([manipulate_supply/0]).

-spec manipulate_supply() -> {_,_}.
manipulate_supply() ->
  S = order:generate_supply(),
  {Hcpc, Key} = S,
  {Hcpc, Key}.
