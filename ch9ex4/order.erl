-module(order).
-export([generate_supply/0, is_hcpc_excluded/1]).

-type hcpc() :: string().
-type supply_key() :: mask | cushions | filter | misc.
-opaque supply() :: {hcpc(), supply_key()}.
-export_type([supply/0]).

-spec generate_supply() -> supply().
generate_supply() ->
  {"A0701", mask}.

-spec is_hcpc_excluded(supply()) -> boolean().
is_hcpc_excluded(Supply) ->
  {Hcpc, _} = Supply,
  case Hcpc of
    "A0701" = Hcpc ->
      true;
    _ ->
      false
  end.
