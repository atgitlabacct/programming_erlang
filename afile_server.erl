-module(afile_server).
-export([start/1, loop/1]).

start(Dir) -> spawn(afile_server, loop, [Dir]).
loop(Dir) ->
  receive
    {Client, list_dir} ->
      Client ! {self(), file:list_dir(Dir)};
    {Client, {get_file, File}} ->
      Full = filename:join(Dir, File),
      Client ! {self(), file:read_file(Full)};
    {Client, {put_file, {FileName, Content}}} ->
      file:write_file(FileName, Content),
      Client ! {self(), {ok, "DONE"}};
    {Client, _} ->
      Client ! {self(), {ok, "Unknown command"}}
  end,
  loop(Dir).  % Don't worry about stack space here.

% Erlang applies a so called tail-call optiiztion to the code,
% which means that this function will run in constant space.
%
% Also note that in a sequential programming language doing an
% infinite loop is usually a terrible thing.  In Erlang this is
% spawned into a process so it won't hang.
