-module(ch13e4).
-compile(export_all).

loop_running() ->
  receive
  after 5000 ->
          io:format("I'm still running"),
          loop_running()
  end.

register_running() ->
  RunningPid = spawn(?MODULE, loop_running, []),
  register('runner', RunningPid),
  RunningPid.

monitor_running(Pid) ->
  spawn(fun() ->
            monitor(process, Pid),
            receive
              {'DOWN', _Ref, process, Pid2, Reason} ->
                io:format("~p died with reason ~p~n", [Pid2, Reason]),
                io:format("Restarting process...~n", []),
                NewPid = register_running(),
                io:format("Restarting monitor...~n", []),
                NewPid = register_running(),
                monitor_running(NewPid)
            end
        end).
