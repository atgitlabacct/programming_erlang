-module(playtime).
-compile(export_all).

start() ->
  spawn_monitor(?MODULE, loop, []).

rpc(Pid, Request) ->
  Pid ! {self(), Request},
  receive
    {Pid, Response} ->
      Response
  end.

loop() ->
  receive
    Any ->
      io:format("Received: ~p~n",[Any]),
      loop()
  end.
