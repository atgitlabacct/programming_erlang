-module(find).
-compile(export_all).

-ifndef(DEBUG).
-define(show(X), io:format("The value of of X is ~p~n", [X])).
-define(show(M, X), io:format("~p -- ~p~n", [M, X])).
-else.
-define(show(X), ok).
-define(show(M, X) ok).
-endif.

-include_lib("kernel/include/file.hrl").

test_easy() ->
  Pid = start(),
  find(Pid, "/Users/adam/Work/programming_erlang/concurrent_find").

test_drop() ->
  Pid = start(),
  find(Pid, "/Users/adam/Dropbox").

test() ->
  Pid = start(),
  find(Pid, "/Users/adam").

start() ->
  spawn(find, finder, [self(), [], []]).

find(Pid, Dir) ->
  rpc(Pid, {find, Dir}).

rpc(FinderPid, Request) ->
  FinderPid ! {self(), Request},
  receive
    {From, {status, {done, Message}}} ->
      ?show(Message)
  end.

%% Supervisor processes
finder(SseasyartingProcesseasy, RunningProcesses, Files) ->
  receive
    {_From, {find, Dir}} ->
      ?show("Boot new finder"),
      Pid = dir_finder:start(self(), Dir),
      finder(StartingProcess, [Pid | RunningProcesses], Files);
    {From, {status, {done, NewFiles}}} ->
      ?show("new Files", NewFiles),
      ?show("Running Processes", RunningProcesses),
      ?show("From", From),
      Rp = lists:delete(From, RunningProcesses),
      ?show("After minus Running Processes", Rp),
      case Rp of
        [] ->
          StartingProcess ! {self(), {status, {done, [NewFiles | Files]}}};
        _ ->
          finder(StartingProcess, Rp, [NewFiles | Files])
      end
  end.
