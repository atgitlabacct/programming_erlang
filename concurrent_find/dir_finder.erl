-module(dir_finder).
-compile(export_all).

-include_lib("kernel/include/file.hrl").

-ifndef(DEBUG).
-define(show(X), io:format("The value of of X is ~p~n", [X])).
-define(show(M, X), io:format("~p -- ~p~n", [M, X])).
-else.
-define(show(X), ok).
-define(show(M, X) ok).
-endif.


start(From, Dir) ->
  spawn(dir_finder, find, [From, Dir]).

find(From, Dir) ->
  {ok, Files} = file:list_dir(Dir),
  find(From, Dir, Files, []).

find(From, Dir, [], Acc) ->
  From ! {self(), {status, {done, Acc}}};

find(From, Dir, [H | Files], Acc) ->
  case file_type(filename:join(Dir,H)) of
    regular ->
      find(From, Dir, Files, [H | Acc]);
    directory ->
      ?show("Dir is ", H),
      From ! {self(), {find, filename:join(Dir, H)}},
      find(From, Dir, Files, Acc);
    error ->
      find(From, Dir, Files, Acc)
  end.

file_type(F) ->
  case file:read_file_info(F) of
    {ok, FileInfo} ->
      case FileInfo#file_info.type of
        regular -> regular;
        directory -> directory;
        _ ->
          error
      end;
    _ ->
      error
  end.
