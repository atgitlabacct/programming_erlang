-module(ch16ex1).
-compile(export_all).

-include_lib("kernel/include/file.hrl").

test() ->
  {error, "Not a regular file"} = files_needs_compiling("/Users/adam"), % Directory should error
  "./foo.beam" = make_beam_name("foo.erl"),
  {true, _} = files_needs_compiling("/Users/adam/Work/programming_erlang/a.erl"),
  {error, _} = files_needs_compiling("/Users/adam/Work/programming_erlang/fgg.erl"),
  test_worked.

files_needs_compiling(File) ->
  case is_file(File) of
    {ok, File} ->
      needs_recompile(File);
    {error, Message} ->
      {error, Message}
  end.

is_file(File) ->
  case file:read_file_info(File) of
    {ok, Facts} ->
      case Facts#file_info.type of
        regular -> {ok, File};
        _ -> {error, "Not a regular file"}
      end;
    _ -> {error, "Could not read file info"}
  end.

needs_recompile(File) ->
  S1 = calendar:datetime_to_gregorian_seconds(last_modified(File)),
  S2 = calendar:datetime_to_gregorian_seconds(last_modified(make_beam_name(File))),
  if
    S1 > S2 ->
      {true, File};
    true ->
      {false, File}
  end.

last_modified(File) ->
  case file:read_file_info(File) of
    {ok, ErlFileInfo} ->
      ErlFileInfo#file_info.mtime;
    _ -> {'error', "Error occurred"}
  end.

make_beam_name(File) ->
  NoExtName = filename:basename(File, '.erl'),
  BeamName = string:concat(NoExtName, ".beam"),
  filename:join([filename:dirname(File), BeamName]).

