-module(finder).
-compile(export_all).

-include_lib("kernel/include/file.hrl").

find(Path) ->
  {ok, Listing} = file:list_dir(Path),
  Files = find_only_files(Listing, []),
  io:format("Files: ~p~n", [ Files ]).

find_only_files([], Acc) ->
  Acc;

find_only_files([Listing | T], Acc) ->
  IsFile = is_file(T),
  io:format("IsFile: ~p, File: ~p~n", [IsFile, T]),
  if
    IsFile ->
      find_only_files(Listing, [T | Acc]);
    true -> 
      find_only_files(Listing, Acc)
  end.

is_file(File) ->
  {ok, FileInfo} = file:read_file_info(File),
  case FileInfo#file_info.type of
    regular ->
      true;
    _ ->
      false
  end.
