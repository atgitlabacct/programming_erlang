-module(geometry).
-export([area/1, test/0]).

test() ->
  12 = area({rectangle, 3, 4}),
  144 = area({square, 12}),
  12.56 = area({circle, 2}),
  8.0 = area({triangle, 4, 4}),
  tests_worked.

area({rectangle, Width, Height}) ->
  Width * Height;
area({square, Side}) ->
  Side * Side;
area({circle, Radius}) ->
  3.14 * Radius * Radius;
area({triangle, Base, Height}) ->
  0.5 * Base * Height.
