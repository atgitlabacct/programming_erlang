-module(ch13e6).
-compile(export_all).

start(NProcesses) ->
  spawn(?MODULE, my_monitor, [NProcesses]).

my_monitor(N) ->
  PidsRefs = [spawn_monitor(?MODULE, loop, []) || _X <- lists:seq(1,N)],
  Pids = [P || {P, _R} <- PidsRefs]s]
  Refs = [R || {_P, R} <- PidsRefs],
  io:format("Pids ~p~n", [Pids]),
  receive
    {'DOWN', Ref, process, DeadPid, Reason} when Reason =/= normal ->
      [demonitor(R) || R <- (Refs -- [Ref])],
      [exit(P, kill) || P <- (Pids -- [DeadPid])],
      my_monitor(N)
  end.

loop() ->
  receive
    {_From, stop} ->
      io:format("All done");
    Any ->
      io:format("Received ~p~n", [Any])
  end.
