-module(ring_benchmark).
-compile(export_all).

start(NumberProcesses, NumberMessages) ->
  Pids = ring_benchmark:generate_processes(NumberProcesses),
  ring_benchmark:send_messages(NumberMessages, Pids),
  Pids.

stop(Pids) ->
  lists:foreach(fun(Elem) -> ring_benchmark:rpc(Elem, stop) end, Pids).

generate_processes(N) ->
  generate_processes(N, []).

generate_processes(N, L) when N =:= 0 ->
  L;
generate_processes(N, L) when N > 0 ->
  Pid = spawn(?MODULE, loop, []),
  generate_processes(N - 1, [Pid| L]).

rpc(Pid, Request) ->
  Pid ! {self(), Request},
  receive
    {Pid, Response} ->
      Response
  end.

loop() ->
  receive
    {_, _, TimesLeft} when TimesLeft =:= 0 ->
      io:format("All done"),
      loop();
    {OriginalList, [], TimesLeft} ->
      [To | Rest] = OriginalList,
      io:format("Sending to ~p from ~p -- remaining ~p~n", [To, self(), []]),
      To ! {OriginalList, Rest, TimesLeft - 1},
      loop();
    {OriginalList, [_H | []], TimesLeft} ->
      [To | Rest] = OriginalList,
      io:format("Sending to ~p from ~p -- remaining ~p~n", [To, self(), []]),
      To ! {OriginalList, Rest, TimesLeft - 1},
      loop();
    {OriginalList, [To | T], TimesLeft} ->
      io:format("Sending to ~p from ~p -- remaining ~p~n", [To, self(), T]),
      To ! {OriginalList, T, TimesLeft - 1},
      loop();
    {From, stop} ->
      io:format("Stopping ~p~n", [self()]),
      ring_benchmark:rpc(From, {ok, "Stopping"});
    Any ->
      io:format("Received: ~p~n", [Any]),
      loop()
  end.

send_messages(N, L) ->
  [H | T] = L,
  H ! {L, T, N}.
