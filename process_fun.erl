-module(process_fun).
-compile(export_all).

start() ->
  spawn(process_fun, loop, []).

rpc(Pid, Request) ->
  Pid ! {self(), Request},
  receive
    {Pid, Response} ->
      io:format("Response was " ++ Response)
  after
    2000 ->
      io:format("Timeout")
  end.

loop() ->
  receive
    {From, talk, Message} ->
      From ! {self(), talk, "I got your message of " ++ Message},
      loop();
    {From, stop} ->
      From ! {self(), talk, "I am stopping"}
  end.

