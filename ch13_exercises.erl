-module(ch13_exercises).
-compile(export_all).

-import(calendar, [local_time/0]).

my_spawn(Mod, Func, _Args, Time) ->
  StartTime = local_time(),
  Pid = spawn(Mod, Func, [ Time ]),
  gen_monitor(Pid, fun(P, Reason) ->
                       io:format("~p died with ~p~n", [P, Reason]),
                       io:format("Ran for ~p~n", [calendar:time_difference(StartTime, local_time())])
                   end),
  Pid.

gen_monitor(Pid, Fun) ->
  spawn(fun() ->
            monitor(process, Pid),
            receive
              {'DOWN', _Ref, process, Pid2, Reason} ->
                Fun(Pid2, Reason)
            end
        end).

loop(Time) ->
  {_, _, Seconds} = Time,
  receive
    {_From, kill} ->
      list_to_atom(foo);
    Any ->
      io:format("Received: ~p~n", [Any]),
      loop(Time)
  after
    Seconds * 1000 ->
        exit(self(), kill)
  end.

test_my_spawn() ->
  my_spawn(?MODULE, loop, [], {0,0,infinity}).

test_my_spawn(Time) ->
  my_spawn(?MODULE, loop, [], Time).


loop_running() ->
  receive
  after 5000 ->
          io:format("I'm still running"),
          loop_running()
  end.

register_running() ->
  RunningPid = spawn(?MODULE, loop_running, []),
  register('runner', RunningPid),
  RunningPid.

monitor_running(Pid) ->
  spawn(fun() ->
            monitor(process, Pid),
            receive
              {'DOWN', _Ref, process, Pid2, Reason} ->
                io:format("~p died with reason ~p~n", [Pid2, Reason]),
                NewPid = register_running(),
                monitor_running(NewPid)
            end
        end).
