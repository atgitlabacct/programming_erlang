-ifndef(DEBUG).
-define(show(X), io:format("The value of of X is ~p~n", [X])).
-define(show(M, X), io:format("~p -- ~p~n", [M, X])).
-else.
-define(show(X), ok).
-define(show(M, X) ok).
-endif.


