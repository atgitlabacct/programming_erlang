-module(lib_misc).
-compile(export_all).

for(Max, Max, F) -> [F(Max)];
for(I, Max, F) -> [F(I)|for(I+1, Max, F)].

qsort([]) -> [];
qsort([Pivot|T]) ->
  qsort([X || X <- T, X < Pivot])
  ++ Pivot ++
  qsort([X || X <- T, X >= Pivot]).

pythag(N) ->
  [{A,B,C} || 
    A <- lists:seq(1, N),
    B <- lists:seq(1, N),
    C <- lists:seq(1, N),
    A+B+C =< N,
    A*A+B*B =:= C*C].

perms([]) -> [[]];
perms(L) -> 
  [[H|T] || H <- L, T <- perms(L -- [H])].

% H = b -> (L -- H) = at
%   -> H = a -> (L -- H) = t
%   -> H = t -> (L -- H) = a
% H = a -> (L -- H) = bt
%   -> H = b -> (L -- H) = t
%   -> H = t -< (L -- H) = b
% t
%
% c -> (L--H) = ats
%   -> a -> (L--H) = ts
%     -> t -> s
%     -> s -> t
%   -> t -> (L--H) = as
%     -> a -> s
%     -> s -> a
%   -> s -> (L--H) = at
%     -> a -> t
%     -> t -> a

%
% a
% t
% s
%
odds_and_evens2(L) ->
  odds_and_evens_acc(L, [], []).
odds_and_evens_acc([H|T], Odds, Evens) ->
  case (H rem 2) of
    1 -> odds_and_evens_acc(T, [H|Odds], Evens);
    0 -> odds_and_evens_acc(T, Odds, [H|Evens])
  end;
odds_and_evens_acc([], Odds, Evens) ->
  {lists:reverse(Odds), lists:reverse(Evens)}.

sqrt(X) when X < 0 ->
  error({squareRootNegativeArgument, X});
sqrt(X) ->
  math:sqrt(X).

sleep(T) ->
  receive
  after T ->
          true
  end.

% Erlang process will try and read its "mailbox" first.
% The after 0 means to execute immediately.
% So this reads any messages in the buffer and then returns true
flush_buffer() ->
  receive
    _Any ->
      flush_buffer()
  after 0 ->
          true
  end.

priority_receive() ->
  receive
    {alarm, X} ->
      {alarm, X}
  after 0 ->
    receive
      Any ->
        Any
    end
  end.

on_exit(Pid, Fun) ->
  spawn(fun() ->
            Ref = monitor(process, Pid),
            receive
              {'DOWN', Ref, process, Pid, Why} ->
                Fun(Why)
            end
        end).

test_on_exit() ->
  % spawn a process
  F = fun() ->
          receive
            X -> 
              list_to_atom(X)
          end
      end,
  Pid = spawn(F),
  % Monitor that process and execure anon function if it exists.
  E = fun(Why) ->
          io:format("~p died with reason ~p", [Pid, Why])
      end,
  lib_misc:on_exit(Pid, E),
  % Send a non list to Pid
  Pid ! "foo".

keep_alive(Name, Fun) ->
  register(Name, Pid = spawn(Fun)),
  on_exit(Pid, fun(_Why) ->
                   keep_alive(Name, Fun) end).

consult(File) -> 
  case file:open(File, read) of
    {ok, S} ->
      Val = consult1(S),
      file:close(S),
      {ok, Val};
    {error, Why} ->
      {error, Why}
  end.

consult1(S) ->
  case io:read(S, '') of
    {ok, Term} -> [Term|consult1(S)];
    eof -> [];
    Error -> Error
  end.

unconsult(File, L) ->
  {ok, S} = file:open(File, write),
  lists:foreach(fun(X) -> io:format(S, "~p.~n", [X]) end, L),
  file:close(S).

-include_lib("kernel/include/file.hrl").
file_size_and_type(File) ->
  case file:read_file_info(File) of
    {ok, Facts} ->
      {Facts#file_info.type, Facts#file_info.size};
    _ ->
      error
  end.

ls(Dir) ->
  {ok, L} = file:list_dir(Dir),
  lists:map(fun(I) -> {I, fs
qsort
qsort([]) -> [];
qsort([Pivot|T]) ->
  qsort([X || X <- T, X < Pivot])
  ++ Pivot ++
  qsort([X || X <- T, X >= Pivot]).

pythag(N) ->
  [{A,B,C} || 
    A <- lists:seq(1, N),
    B <- lists:seq(1, N),
    C <- lists:seq(1, N),
    A+B+C =< N,
    A*A+B*B =:= C*C].

perms([]) -> [[]];
perms(L) -> 
  [[H|T] || H <- L, T <- perms(L -- [H])].

([]) -> [];
qsort([Pivot|T]) ->
  qsort([X || X <- T, X < Pivot])
  ++ Pivot ++
  qsort([X || X <- T, X >= Pivot]).

pythag(N) ->
  [{A,B,C} || 
    A <- lists:seq(1, N),
    B <- lists:seq(1, N),
    C <- lists:seq(1, N),
    A+B+C =< N,
    A*A+B*B =:= C*C].

perms([]) -> [[]];
perms(L) -> 
  [[H|T] || H <- L, T <- perms(L -- [H])].

le_size_and_type(I)} end,
            lists:sort(L)).
