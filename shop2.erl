-module(shop2).
-export([total/1]).

-import(lists, [map/2, sum/1]).

total(L) ->
  sum(map(fun({Name, Quantity}) ->
          shop:cost(Name) * Quantity end, L)).
