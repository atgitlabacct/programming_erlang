-module(ch9ex1).
-export([calculate_patient_status/2]).

-type order() :: {string(), orderStatus(), orderType(), calendar:date()}.
-type orderStatus() :: open | closed.
-type orderType() :: pickup | delivery.
-type patient() :: {string, patientStatus()}.
-type patientStatus() :: active | inactive | unreachable | deceased | loading | new.

-spec calculate_patient_status(patient(), order()) -> patient().

calculate_patient_status(P, O) ->
  {_orderKey, OrderStatus, OrderType, _date} = O,
  case P of
    {_Name, new} ->
      from_new(P, OrderStatus, OrderType);
    {_Name, active} ->
      from_active(P, OrderType)
  end.

from_new(Patient, closed, delivery) ->
  {Name, _status} = Patient,
  {Name, active};
from_new(Patient, _, _) ->
  Patient.

from_active(Patient, OrderType) ->
  {Name, _status} = Patient,
  case OrderType of
    pickup ->
      {Name, inactive};
    _ ->
      Patient
  end.
