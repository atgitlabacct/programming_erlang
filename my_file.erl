-module(my_file).
-export([read/1]).

read(File) ->
  case file:read_file(File) of
    {error, Message} -> error({errorReadingFile, Message, erlang:get_stacktrace()});
    {ok, Message} -> {ok, Message}
  end.

