-module(mailbox).
-compile(export_all).


start() -> spawn(mailbox, slow_mailbox, []).
startd() -> spawn(mailbox, delayed_mailbox, []).

start_test_mailbox() -> 
  Pid = spawn(mailbox, testing_mailbox, []),
  register(test_mailbox, Pid),
  Pid.

-type mailbox_commands() :: foo | bar | stop.
-spec send_to_mailbox(pid(), mailbox_commands()) -> any().
send_to_mailbox(To, Command) ->
  test_mailbox ! {self(), Command},
  receive
    {To, Response} ->
      {Code, Value} = Response,
      io:format("Response was ~s~n", [Value]);
    Any ->
      {Pid, {ok, Value}} = Any,
      io:format("Test Mailbox -- ~p~n", [test_mailbox]),
      io:format("Pid Back -- ~p~n", [Pid]),
      io:format("Value Back -- ~p~n", [Value])
  after 2001 ->
        io:format("Did not receive response~n")
  end.

testing_mailbox() ->
  receive
    {From, foo} ->
      From ! {self(), {ok, "foo"}},
      testing_mailbox();
    {From, bar} ->
      From ! {self(), {ok, "bar"}},
      testing_mailbox();
    {From, stop} ->
      From ! {self(), {ok, "stopping"}}
  end.
% Executing this quickly after starting up the slow_mailbox will timeout.
% The receive will wait 5 seconds before timing out and the "Slept"
% message will never make it in time.
slowdown(Pid) -> 
  Pid ! {self(), slow},
  receive
    {Pid, Message} -> 
      io:format("Message: ~p~n", [ Message ]);
    _Any ->
      _Any
  after 5000 ->
          io:format("Timeout", [])
  end.

% With the timeout in slow_mailbox match will take a 5 seconds to output.
% So I can send 5 quick messages after start() and it will take approx 25 seconds
% for the output to hit the screen.  This is because receive only reads one message
% at a time.
match(Pid) ->
  Pid ! {self(), match}.

slow_mailbox() ->
  timer:sleep(5000),
  receive
    {From, slow} ->
      From ! {self(), "Slept"};
    {From, match} ->
      io:format("We matched~n", []),
      slow_mailbox();
    {From, Other} ->
      From ! {self(), {error, Other}}
  end.

% If a message in the mailbox does not match a receive pattern the process gets set to suspending
% and waits till a new message hits the mailbox until it processes again.
% I can do Pid ! {self(), foo} 10 times and nothing happens; but the minute I 
% do Pid ! {self(), message} the process picks back up and executes the pattern match.
delayed_mailbox() ->
  receive
    {From, message} ->
      io:format("Some message~n", [])
  end.
