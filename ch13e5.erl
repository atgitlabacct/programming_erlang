-module(ch13e5).
-compile(export_all).

start() ->
  MPid = start_monitor(),
  Pids = start_processes(10),
  add_list_to_monitor(MPid, Pids),
  {MPid, Pids}.

start_monitor() ->
  spawn(?MODULE, global_monitor, []).

spawn_process() ->
  spawn(?MODULE, my_process, []).

start_processes(N) ->
  start_process(N-1, [spawn_process()]).

start_process(N, L) when N =:= 0 -> L;
start_process(N, L) when N > 0 ->
  io:format("List ~p~n", [L]),
  start_process(N-1, [spawn_process() | L]).

add_list_to_monitor(Monitor, []) ->
  Monitor;
add_list_to_monitor(Monitor, Pids) ->
  [H | T] = Pids,
  add_to_monitor(Monitor, H),
  add_list_to_monitor(Monitor, T).

add_to_monitor(Monitor, PidToWatch) ->
  rpc(Monitor, {add_monitor, PidToWatch}).

rpc(Pid, Request) ->
  Pid ! {self(), Request},
  receive
    {Pid, Response} ->
      Response
  after 2000 ->
        {done, "Two seconds passed"}
  end.

my_process() ->
  receive
    Any ->
      io:format("Received: ~p~n", [Any])
  end.

global_monitor() ->
  receive
    {From, {add_monitor, PidToWatch}} ->
      monitor(process, PidToWatch),
      io:format("Now monitoring ~p~n", [PidToWatch]),
      From ! {self(), {ok, "Added to monitor"}},
      global_monitor();
    {'DOWN', _Ref, process, _DeadPid, _Reason} ->
      {Pid, _} = spawn_monitor(?MODULE, my_process, []),
      io:format("Starting new process ~p and monitoring with ~p~n", [Pid, self()]),
      global_monitor();
    Any ->
      io:format("Received: ~p~n", [Any]),
      global_monitor()
  end.
