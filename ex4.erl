-module(ex4).
-export([my_tuple_to_list/1, test/0, my_time_func/1, my_date_string/0]).

test() ->
  [d,c,b,a] = my_tuple_to_list({a,b,c,d}),
  tests_worked.

my_tuple_to_list({}) -> [];
my_tuple_to_list(T) ->
  my_tuple_to_new_list(T, 2, [element(1, T)]).

my_tuple_to_new_list(T, Size, L) when Size > tuple_size(T) -> L;
my_tuple_to_new_list(T, Size, L) when Size =< tuple_size(T) ->
  my_tuple_to_new_list(T, Size+1, [element(Size, T)|L]).

my_time_func(F) ->
  {PrevMega, PrevSec, PrevMs} = now(),
  F(),
  {NowMega, NowSec, NowMs} = now(),
  {NowMega - PrevMega, NowSec - PrevSec, NowMs - PrevMs}.

my_date_string() ->
  {Year, Month, Day} = date(),
  io:fwrite("Date is: ~w-~w-~w.\n", [Year, Month, Day]).
