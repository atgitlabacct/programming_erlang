-module(math_functions).
-export([test/0, even/1, odd/1, filter/2, split/1, split2/1]).

test() ->
  true = even(10),
  false = even(11),
  true = odd(11),
  false = odd(10),
  {[2,4,6], [1,3,5]} = split2([1,2,3,4,5,6]),
  tests_passed.

even(N) ->
  if 
    N rem 2 =:= 0 ->
      true;
    true -> false
  end.

odd(N) -> 
  if
    N rem 2 =:= 0 ->
      false;
    true -> true
  end.

filter(F, L) ->
  [X || X <- L, F(X) =:= true].

split(L) ->
  {[X || X <- L, even(X)],
   [Y || Y <- L, odd(Y)]}.

split2(L) ->
  split_acc(L, [], []).
split_acc([H|T], Evens, Odds) ->
  case even(H) of
    true -> split_acc(T, [H | Evens], Odds);
    false -> split_acc(T, Evens, [H | Odds])
  end;
split_acc([], Evens, Odds) ->
  {lists:reverse(Evens), lists:reverse(Odds)}.
